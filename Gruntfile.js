module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    path: {
      src: "src",
      dev: "dev",
      dist: "dist"
    },
    files: {
        coffee: "<%= path.src %>/coffee/*.coffee"
    },
    coffee: {
      glob_to_multiple: {
          expand: true,
          flatten: true,
          cwd: '<%= path.src %>/coffee/',
          src: ['*.coffee'],
          dest: '<%= path.dev %>/js',
          ext: '.js'
        }
    },

    watch: {
      files: ['<%= files.coffee %>'],
      tasks: ['coffee', 'browserify']
    },
    browserify: {
      main: {
        src: './dev/js/index.js',
        dest: './dev/js/bundle.js'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browserify');

  grunt.registerTask('default', ['coffee', 'browserify', 'watch']);

};
