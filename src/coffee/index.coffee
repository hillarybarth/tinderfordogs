config = require('../../config.json')
ractive = require('ractive')

#-------------------------------------------------------------------------------
# Petfinder
#-------------------------------------------------------------------------------

requestURL = 'http://api.petfinder.com/pet.find?location=98033&format=json&key=' + config.apiKey + '&callback=?'

petFinder =
  petsList: [],
  populateList: (data)->
      Pet = ->
        id: ''
        name: ''
        animal: ''
        sex: ''
        breeds: ''
        size: ''
        photos: []

      pets = data.petfinder.pets.pet

      for p in pets
        pet = new Pet()
        pet.id = p.id.$t
        pet.name = p.name.$t
        pet.animal = p.animal.$t
        pet.sex = p.sex.$t
        pet.breeds = p.breeds
        pet.size = p.size.$t
        pet.photos = []
        if (p.media.photos != undefined and p.media.photos["photo"].length > 0)
          for photo in p.media.photos["photo"]
            if photo["@size"] == "x"
              pet.photos.push(photo["$t"])
        else
          pet.photos.push('This pet does not have a photo.')
        this.petsList.push(pet)
      return this.petsList

    getRandomPet: ->
      if this.petsList == undefined
        return
      return this.petsList[Math.round(Math.random() * this.petsList.length)]

p = petFinder

#-------------------------------------------------------------------------------
# Local Storage
#-------------------------------------------------------------------------------
# Try to find previous data
if localStorage.tinderForDogs
  console.log('I have been here before!')
  storage = localStorage.getItem('tinderForDogs')
  data = JSON.parse(storage)

#-------------------------------------------------------------------------------
# Ractive
#-------------------------------------------------------------------------------
ractive = new Ractive
  template: '#template'
  el: '#container'
  oninit: ->
    self = this

    if not storage
      $.getJSON requestURL
      .done (data) ->
        self.set('list', p.populateList(data))
      .error (error) ->
        console.log(error)
      .then ->
        toStore =
          list: self.get('list')
          viewed: []
          liked: []
        localStorage.setItem('tinderForDogs', JSON.stringify(toStore))
        @set('pet', p.getRandomPet())  
    else
      p.petsList = data.list
      @set('list', data.list)
      @set('viewed', data.viewed)
      @set('liked', data.liked)

      @set('pet', p.getRandomPet())

  storeData: ->
      toStore =
        list: @get('list')
        viewed: @get('viewed')
        liked: @get('liked')
      localStorage.setItem('tinderForDogs', JSON.stringify(toStore))

  data:
    pet:
      name: 'Agatha'
    list: []
    viewed: []
    liked: []

ractive.on
  swipeLeft: (event, id)->
    console.log 'Swipe left!', id
    @push('viewed', id)
    @storeData()
    ractive.set('pet', p.getRandomPet())
  swipeRight: (event, id) ->
    console.log('Swipe right!', id)
    @push('viewed', id)
    @push('liked', id)
    @storeData()
    ractive.set('pet', p.getRandomPet())
