(function() {
  var Pet, config, findPet, petsList, petsViewed, requestURL;

  config = require('../..config.json');

  requestURL = 'http://api.petfinder.com/pet.find?location=98033&format=json&key=' + apiKey + '&callback=?';

  Pet = function() {
    return {
      name: '',
      animal: '',
      sex: '',
      breeds: '',
      size: '',
      photos: []
    };
  };

  petsList = [];

  petsViewed = [];

  findPet = function() {
    return $.getJSON(requestURL).done(function(data) {
      var i, len, p, pet, pets;
      pets = data.petfinder.pets.pet;
      for (i = 0, len = pets.length; i < len; i++) {
        p = pets[i];
        pet = new Pet();
        pet.name = p.name.$t;
        pet.animal = p.animal.$t;
        pet.sex = p.sex.$t;
        pet.breeds = p.breeds;
        pet.size = p.size.$t;
        pet.photos = p.media.photos;
        petsList.push(pet);
      }
      return console.log(petsList);
    }).error(function(error) {
      return console.log(error);
    });
  };

  findPet();

}).call(this);
